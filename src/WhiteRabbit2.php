<?php

class WhiteRabbit2
{
    //An array that holds the available coins for use. 
    //We can add more coins of different values, as long as we keep the array sorted.
    private $availableCoins = ['1','2','5','10','20','50','100'];
            
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     * @param $amount
     */
    public function findCashPayment($amount){
        if($amount >= 0)
        {
            $amountLeft = $amount;
            for($i = count($this->availableCoins) - 1; $i >= 0; $i--) 
            {
                if($amountLeft > 0)
                {
                    $intDivision = intdiv($amountLeft,$this->availableCoins[$i]);
                
                    if($intDivision >=1)
                    {
                        $requiredCoins[$this->availableCoins[$i]] = $intDivision;
                        $amountLeft -= $intDivision*$this->availableCoins[$i];
                    }
                    else $requiredCoins[$this->availableCoins[$i]] = 0;
                }
                else
                {                   
                    $requiredCoins[$this->availableCoins[$i]] = 0;                            
                }
            }
            ksort($requiredCoins);
            return $requiredCoins;
        }
        else
        {
            echo ("<pre>Invalid amount. Amount must be 0 or bigger!</pre>");
            exit;
        }
    }
}