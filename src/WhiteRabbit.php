<?php

class WhiteRabbit
{    
	public function findMedianLetterInFile($filePath)
	{
        $parsedFile = $this->parseFile($filePath);
        $medianLetter = $this->findMedianLetter($parsedFile);
        return $medianLetter;
	}
	
	/**
	 * Parse the input file for letters.
	 * @param $filePath
	 */
	private function parseFile ($filePath)
	{
		$file_contents = file_get_contents($filePath);
        
        if ($file_contents === false)
		{
			echo ("<pre>Requested file could not be found!</pre>");
            exit;
		}
		else
		{
			$file_contents = strtolower($file_contents); //Making everything lowercase so we can count all the occurences of each letter
			$alphabet = range('a','z');
			
			foreach($alphabet as $letter)
			{
                $foundLetters = false;  //Boolean for checking if we found any letters at all in the given file
				$occurencies = substr_count($file_contents,$letter);
           
                if ($occurencies > 0)
                {
                    $parsedFile[$letter] = $occurencies;
                    $foundLetters = true;
                    
                }
			}
		}
		        
        if($foundLetters)
        {
            return $parsedFile;
        }
        else
        {
            echo "<pre>File is empty or does not contain letters!</pre>";
            exit;
        }
	}
	
	/**
	 * Return the letter whose occurrences are the median.
	 * @param $parsedFile
	 */
	private function findMedianLetter($parsedFile) //Removed the &$occurencies parameter, as the $parsedFile parameter contains the occurancies of each letter as well
	{
		asort($parsedFile);        
        $lettersAmount = count($parsedFile); //This is done for smaller files, where some letters might not appear at all
        
        if($lettersAmount % 2 != 0)
        {
            $median = (($lettersAmount+1)/2); //Median formula for odd number of letters
        }
        else
        {
            $median = $lettersAmount/2 ; //Since we can find the mean of 2 letters(the middle ones), I arbitrarily chose one of them
        }
        
        $letters = array_keys($parsedFile);
        $occurencies = array_values($parsedFile);
        
        $medianLetter = array("letter"=>$letters[$median-1],"count"=>$occurencies[$median-1]);

        return $medianLetter;
	}
    
}